# [WAT website](https://wgt.rakshazi.me) [![GoDoc](https://godoc.org/gitlab.com/rakshazi/wat?status.svg)](https://godoc.org/gitlab.com/rakshazi/wat)

This program will generate your guild's raid attendance list with twink matches

## Algorithm

1. Grabs guild reports from Warcraft Logs API
2. Parses each report from that list
3. Merges alts/twiks to 1 character
4. Export processed data to json

## Configuration

This tool needs a `config.json` file with following structure:

```json
{
    "apikey": "your-warcraftlogs-api-key",
    "export": {
        "format": "csv",
        "path": "./",
        "template: "./tpl.html"
        "templateRaid: "./tpl.raid.html"
    },
    "guilds": [
        {
            "name": "Совет Тирисфаля",
            "realm": "ревущии-фьорд",
            "filename": "tirisfal",
            "region": "eu",
            "raidIds": [24],
            "maxRaids": 10,
            "ignoreDays": [0, 2, 5, 6],
            "ignoreDifficulties": [1, 2, 10],
            "ignoreLower": 13,
            "alts": [
                {
                    "main": "Этке",
                    "alts": ["Сиву", "Зирэль"]
                }
            ]
        }
    ]
}
```

* apikey - [Wacraft Logs API Key](https://www.warcraftlogs.com/profile#api-title) follow the link and create one. **NOTE**: may be set with `WAT_APIKEY` env var
* export - export settings
* * format - export file format one of: `csv`, `html`
* * template - file template for `html` format
* * templateRaid - file sub-template for `html` format
* guilds - list of guilds to work with
* * name - guild name
* * realm - guild realm, Waracraft Logs slug (eg: russian realms must replace "й" to "и")
* * region - guild region
* * filename - custom filename for guild results
* * raidIds - list of raids to work with. 24 - Nyalotha, 23 - The Eternal Palace, 22 - Crucible of Storms, 21 - BoD, 19 - Uldir
* * maxRaids - maximum raids to export (after merge and twink matching)
* * ignoreDays - ignore reports recorded in following days. 0 - Sunday, ... [go time.Weekday](https://golang.org/pkg/time/#Weekday)
* * ignoreLower - ignore characters with attendance percent lower than that value
* * ignoreDifficulties - ignore fights with following difficulties. 1 - LFR, 2 - Flex, 3 - Normal, 4 - Heroic, 5 - Mythic, 10 - Challenge/Keystone/Mythic+
* * alts - list of match pairs. Example: you have main char `Etke` and alts `Sivu` and `Zirel`, but you go to raid with all of these chars,
so RL must respect your alts attendance and merge it with your main's attendance manually. Now, you just set list of your twinks and RL will be happy

You can also specify the config file path with the `-config` flag. For example:

```bash
wat -config my/custom/dir/my_config.json
```

Check more configuration options in the [conf.json.dist](config.json.dist)

## Run

### Golang

```bash
go get gitlab.com/rakshazi/wat
cd $GOPATH/src/gitlab.com/rakshazi/wat
go install
curl https://gitlab.com/rakshazi/wat/raw/master/config.json.dist -o config.json
wat
```

### Binary

```bash
curl https://gitlab.com/rakshazi/wat/-/jobs/artifacts/master/raw/app?job=binary --output wat
curl https://gitlab.com/rakshazi/wat/raw/master/config.json.dist -o config.json
chmod +x ./wat
./wat
```

### Docker

```bash
curl https://gitlab.com/rakshazi/wat/raw/master/config.json.dist -o config.json
docker run -d -v "$PWD/config.json:/opt/app/config.json" registry.gitlab.com/rakshazi/wat
```

