package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

// Config is list of all guilds' configs
type Config struct {
	Key    string `json:"apikey,omitempty"`
	Export Export
	Guilds []Item `json:"guilds"`
}

// Export configuration
type Export struct {
	Format       string `json:"format,omitempty"`
	Path         string `json:"path,omitempty"`
	Template     string `json:"template,omitempty"`
	TemplateRaid string `json:"templateRaid,omitempty"`
}

// Item represents configuration set for 1 guild
type Item struct {
	ID                 string
	Filename           string `json:"filename,omitempty"` // Vanity/Custom URL/file
	Name               string `json:"name"`
	Realm              string `json:"realm"`
	RealmSlug          string
	Progress           map[string]string
	Region             string  `json:"region"`
	IgnoreAtt          float64 `json:"ignoreLower,omitempty"`        // Ignore chars with attendance lower than that number
	IgnoreDifficulties []int   `json:"ignoreDifficulties,omitempty"` // Ignore bosses difficulty
	IgnoreDays         []int64 `json:"ignoreDays,omitempty"`         // Ignore reports, recored in following days. 0 - Sunday...
	Raids              []int64 `json:"raidIds"`                      // 24 - Nyalotha, 23 - The Eternal Palace, 22 - Crucible of Storms, 21 - BoD, 19 - Uldir
	MaxRaids           int     `json:"maxRaids"`
	Alts               []Alt   `json:"alts,omitempty"`
}

// Alt represents list of all characters of one player
type Alt struct {
	Main string   `json:"main"`
	Alts []string `json:"alts"`
}

// Load Returns a new configuration loaded from a file
func Load(configFile string) (Config, error) {
	config := Config{}

	if _, err := os.Stat(configFile); os.IsNotExist(err) {
		log.Println("[config] file", configFile, "not found, using trying wat.json")
		configFile = "wat.json"
	}

	if _, err := os.Stat(configFile); os.IsNotExist(err) {
		return config, fmt.Errorf("[config] file not found")
	}
	log.Printf("[config] found: %s\n", configFile)

	configData, err := ioutil.ReadFile(configFile)
	if err != nil {
		return config, fmt.Errorf("[config] read error: %v", err)
	}

	err = json.Unmarshal(configData, &config)
	if err != nil {
		return config, fmt.Errorf("[config] decode error: %v", err)
	}
	if config.Key == "" {
		if os.Getenv("WAT_APIKEY") == "" {
			return config, fmt.Errorf("[config] no WarcraftLogs.com API key found. You must set it with config 'apikey' key or with 'WAT_APIKEY' env var")
		}
		config.Key = os.Getenv("WAT_APIKEY")
	}
	// Prepare config values for WoW Progress
	for id, guild := range config.Guilds {
		config.Guilds[id].RealmSlug = NormalizeRealm(guild.Realm)
		config.Guilds[id].ID = guild.Name + "/" + config.Guilds[id].RealmSlug + "/" + guild.Region
		if guild.Filename == "" {
			config.Guilds[id].Filename = config.Guilds[id].ID
		}
	}

	log.Println("[config] loaded successfully")
	return config, nil
}
