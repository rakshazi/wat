package config

// Pairs of (left) WarcraftLogs' realm slugs and (right) standard slugs
var transliteration = map[string]string{
	"ясеневыи-лес":     "ashenvale",
	"азурегос":         "azuregos",
	"черныи-шрам":      "blackscar",
	"пиратская-бухта":  "booty-bay",
	"бореиская-тундра": "borean-tundra",
	"страж-смерти":     "deathguard",
	"ткач-смерти":      "deathweaver",
	"подземье":         "deepholm",
	"вечная-песня":     "eversong",
	"дракономор":       "fordragon",
	"галакронд":        "galakrond",
	"голдринн":         "goldrinn",
	"гордунни":         "gordunni",
	"седогрив":         "greymane",
	"гром":             "grom",
	"ревущии-фьорд":    "howling-fjord",
	"король-лич":       "lich-king",
	"разувии":          "razuvious",
	"свежеватель-душ":  "soulflayer",
	"термоштепсель":    "thermaplugg",
}

// NormalizeRealm convert very fcking special WarcraftLogs ream slug to standard realm slug
func NormalizeRealm(realm string) string {
	if _, ok := transliteration[realm]; !ok {
		return realm
	}

	return transliteration[realm]
}
