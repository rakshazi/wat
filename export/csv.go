package export

import (
	"encoding/csv"
	"log"
	"os"
	"strconv"

	"gitlab.com/rakshazi/wat/config"
	"gitlab.com/rakshazi/wat/parse"
)

// CSV Export
func CSV(guild *config.Item, reports []*parse.ParsedReport, path string) error {
	filename := getFilename(path, guild.Filename, "csv")

	headers := getCSVHeaders(reports)
	items := getCSVItems(guild, reports)

	file, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer file.Close()

	writer := csv.NewWriter(file)
	defer writer.Flush()

	if err := writer.Write(headers); err != nil {
		return err
	}

	for _, line := range items {
		if err := writer.Write(line); err != nil {
			return err
		}
	}

	log.Println("[export/csv] saved", filename)

	return nil
}

// Generate CSV headers by reports' info
func getCSVHeaders(reports []*parse.ParsedReport) []string {
	headers := []string{"Character", "Att. by Night", "Att. by Pull"}

	for _, report := range reports {
		headers = append(headers, report.Date.Format("02/01"))
	}

	return headers
}

// Generate CSV body lines with raiders attendance
func getCSVItems(guild *config.Item, reports []*parse.ParsedReport) [][]string {
	var data [][]string
	characters := parse.GetAllCharacters(guild, reports)
	for _, character := range characters {
		attendanceByNight, attendanceByPull := parse.Attendance(character, reports)
		if attendanceByNight < guild.IgnoreAtt {
			continue
		}
		line := make([]string, len(reports)+3)
		line[0] = character
		line[1] = strconv.FormatFloat(attendanceByNight, 'f', 2, 64)
		line[2] = strconv.FormatFloat(attendanceByPull, 'f', 2, 64)
		for i, report := range reports {
			if parse.CharacterExists(character, report) {
				line[i+3] = strconv.Itoa(parse.GetCharacterPulls(character, report)) + "/" + strconv.Itoa(len(report.Pulls))
			} else {
				line[i+3] = "0"
			}
		}
		data = append(data, line)
	}

	return sortByIndex(data, 1)
}
