package export

import (
	"os"
	"path/filepath"
	"sort"
	"strconv"

	"github.com/gosimple/slug"
)

// Generate filename without subdir
func getFilename(path string, filename string, ext string) string {
	return filepath.Join(path, slug.Make(filename)+"."+ext)
}

// Create new file with subdir
func createFile(path string, dir string, filename string, ext string) (*os.File, error) {
	if _, err := os.Stat(path + slug.Make(dir)); err != nil {
		if os.IsNotExist(err) {
			err = os.Mkdir(path+slug.Make(dir), 0755)
			if err != nil {
				return nil, err
			}
		}
	}

	filename = filepath.Join(path, slug.Make(dir), slug.Make(filename)+"."+ext)
	file, err := os.Create(filename)
	if err != nil {
		return nil, err
	}

	return file, nil
}

// Sort [characters line with attendance] by index
func sortByIndex(slice [][]string, index int) [][]string {
	sort.Slice(slice, func(i, j int) bool {
		// We want to sort by attendance, that's float
		iItem, _ := strconv.ParseFloat(slice[i][index], 64)
		jItem, _ := strconv.ParseFloat(slice[j][index], 64)
		return iItem > jItem
	})
	return slice
}
