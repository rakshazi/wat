package export

import (
	"log"
	"strconv"
	"text/template"

	"gitlab.com/rakshazi/wat/config"
	"gitlab.com/rakshazi/wat/parse"
)

//HTMLFeedItem - item of the feed
type HTMLFeedItem struct {
	Title      string
	Path       string
	Bosses     []*parse.Boss
	Attendance map[string]map[string]int
}

// HTML Export
func HTML(guild *config.Item, reports []*parse.ParsedReport, export *config.Export) error {
	headers := getHTMLHeaders(reports)
	items := getHTMLItems(guild, reports)
	feed := getHTMLFeed(reports)

	tpl, err := template.ParseFiles(export.Template)
	if err != nil {
		return err
	}

	file, err := createFile(export.Path, guild.Filename, "index", "html")
	if err != nil {
		return err
	}
	defer file.Close()

	vars := struct {
		Title   string
		Guild   *config.Item
		Headers []string
		Items   [][]string
		Feed    []*HTMLFeedItem
	}{
		Title:   guild.Name,
		Guild:   guild,
		Headers: headers,
		Items:   items,
		Feed:    feed,
	}
	err = tpl.Execute(file, vars)
	if err != nil {
		return err
	}
	exportHTMLRaids(feed, export.Path, guild.Filename, export.TemplateRaid)

	log.Println("[export/html] saved")

	return nil
}

func exportHTMLRaids(items []*HTMLFeedItem, path string, dir string, tplFile string) error {
	tpl, err := template.ParseFiles(tplFile)
	if err != nil {
		return err
	}
	for _, item := range items {
		file, err := createFile(path, dir, item.Path, "html")
		if err != nil {
			return err
		}
		defer file.Close()
		err = tpl.Execute(file, item)
		if err != nil {
			log.Println("[export/html]", err)
			continue
		}
	}
	return nil
}

// Generate HTML headers by reports' info
func getHTMLHeaders(reports []*parse.ParsedReport) []string {
	headers := []string{"Character", "Att. by Night", "Att. by Pull"}

	for _, report := range reports {
		headers = append(headers, report.Date.Format("02/01"))
	}

	return headers
}

// Generate HTML body lines with raiders attendance
func getHTMLItems(guild *config.Item, reports []*parse.ParsedReport) [][]string {
	var data [][]string
	characters := parse.GetAllCharacters(guild, reports)
	for _, character := range characters {
		attendanceByNight, attendanceByPull := parse.Attendance(character, reports)
		if attendanceByNight < guild.IgnoreAtt {
			continue
		}
		line := make([]string, len(reports)+3)
		line[0] = character
		line[1] = strconv.FormatFloat(attendanceByNight, 'f', 2, 64)
		line[2] = strconv.FormatFloat(attendanceByPull, 'f', 2, 64)
		for i, report := range reports {
			if parse.CharacterExists(character, report) {
				line[i+3] = strconv.Itoa(parse.GetCharacterPulls(character, report)) + "/" + strconv.Itoa(len(report.Pulls))
			} else {
				line[i+3] = "0"
			}
		}
		data = append(data, line)
	}

	return sortByIndex(data, 1)
}

func getHTMLFeed(reports []*parse.ParsedReport) []*HTMLFeedItem {
	var data []*HTMLFeedItem
	for _, report := range reports {
		data = append(data, &HTMLFeedItem{
			Title:      report.Date.Format("02/01"),
			Path:       report.Date.Format("02-01"),
			Bosses:     report.Bosses,
			Attendance: parse.GetAttendanceByReport(report),
		})
	}

	return data
}
