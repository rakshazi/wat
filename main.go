package main

import (
	"flag"
	"log"

	"gitlab.com/rakshazi/warcraftlogs-api"
	"gitlab.com/rakshazi/wat/config"
	"gitlab.com/rakshazi/wat/export"
	"gitlab.com/rakshazi/wat/parse"
	"gitlab.com/rakshazi/wat/reports"
)

func main() {
	configFile := flag.String("config", "config.json", "Path to the configuration file")
	flag.Parse()
	configuration, err := config.Load(*configFile)
	if err != nil {
		log.Fatal(err)
	}
	api := warcraftlogs.New(configuration.Key)
	for _, guild := range configuration.Guilds {
		log.Println("[main] start", guild.ID)
		applyRaidProgress(&guild)
		reportsList, err := reports.Download(api, &guild)
		if err != nil {
			log.Println("[reports]", err, guild.ID)
			continue
		}
		// Shrink reports to requested MaxRaids size
		if len(reportsList) > guild.MaxRaids {
			reportsList = append([]*parse.ParsedReport(nil), reportsList[:guild.MaxRaids]...)
		}
		if configuration.Export.Format == "csv" {
			err = export.CSV(&guild, reportsList, configuration.Export.Path)
			if err != nil {
				log.Println("[export/csv]", err, guild.ID)
			}
		}
		if configuration.Export.Format == "html" {
			err = export.HTML(&guild, reportsList, &configuration.Export)
			if err != nil {
				log.Println("[export/html]", err, guild.ID)
			}
		}
		log.Println("[main] end", guild.ID)
	}
	log.Println("[main] mission completed.")
}

// Add raid progression from raider.io to guild object
func applyRaidProgress(guild *config.Item) {
	progression, err := parse.GetRaidProgression(guild)
	if err != nil {
		log.Println("[main] raider.io failed, ignoring it...")
		return
	}
	guild.Progress = make(map[string]string, len(progression.Progress))
	for name, raid := range progression.Progress {
		guild.Progress[name] = raid.Summary
	}

	log.Println("[main] raider.io progression added")
}
