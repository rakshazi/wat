package parse

import "gitlab.com/rakshazi/wat/config"

// Characters - match characters (main and alts)
func Characters(guild *config.Item, raiders []string) []string {
	// Get unique character names
	var characters []string
	for _, raider := range raiders {
		characters = AppendMissing(characters, matchCharacter(guild, raider))
	}
	return characters
}

// GetAllCharacters - returns list of all characters in ParsedReports
func GetAllCharacters(guild *config.Item, reports []*ParsedReport) []string {
	var characters []string
	for _, report := range reports {
		for _, raider := range report.Raiders {
			characters = AppendMissing(characters, raider)
		}
	}
	return characters
}

// Attendance - calculate character attendance by raid nights and by pulls
func Attendance(character string, reports []*ParsedReport) (float64, float64) {
	all := len(reports)
	pulls := 0
	allPulls := 0
	present := 0
	for _, report := range reports {
		for _, raider := range report.Raiders {
			if raider == character {
				present++
			}
		}
		pulls += GetCharacterPulls(character, report)
		allPulls += len(report.Pulls)
	}
	return (float64(present) / float64(all)) * 100, (float64(pulls) / float64(allPulls)) * 100
}

// GetCharacterPulls - Get count of character pulls on raid
func GetCharacterPulls(character string, report *ParsedReport) int {
	count := 0
	for _, pull := range report.Pulls {
		for _, name := range pull.Raiders {
			if name == character {
				count++
			}
		}
	}

	return count
}

// GetAttendanceByReport - per-raid attendance
func GetAttendanceByReport(report *ParsedReport) map[string]map[string]int {
	attendance := make(map[string]map[string]int)
	pullsCount := len(report.Pulls)
	for _, pull := range report.Pulls {
		for _, raider := range pull.Raiders {
			if _, ok := attendance[raider]; !ok {
				attendance[raider] = make(map[string]int, pullsCount)
			}
			attendance[raider][pull.Name]++
		}
	}

	return attendance
}

// CharacterExists - check if character exists in report
func CharacterExists(character string, report *ParsedReport) bool {
	for _, raider := range report.Raiders {
		if character == raider {
			return true
		}
	}
	return false
}

// Get main character name by alt's name
func matchCharacter(guild *config.Item, character string) string {
	for _, item := range guild.Alts {
		if item.Main == character {
			return item.Main
		}
		for _, alt := range item.Alts {
			if alt == character {
				return item.Main
			}
		}
	}

	return character
}

// AppendMissing only if missing
func AppendMissing(slice []string, character string) []string {
	for _, item := range slice {
		if item == character {
			return slice
		}
	}
	return append(slice, character)
}
