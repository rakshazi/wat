package parse

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	urlPkg "net/url"
	"time"

	"gitlab.com/rakshazi/wat/config"
)

// RaiderIORaidProgressionResponse from API
type RaiderIORaidProgressionResponse struct {
	Progress map[string]*RaiderIORaidProgression `json:"raid_progression"`
}

// RaiderIORaidProgression item from API
type RaiderIORaidProgression struct {
	Summary string
}

// Raider.io API endpoint
var rioAPI = "https://raider.io/api/v1/"
var client = http.Client{
	Timeout: time.Second * 10,
}

// GetRaidProgression - get guild raid progress from Raider.io API
func GetRaidProgression(guild *config.Item) (*RaiderIORaidProgressionResponse, error) {
	url := fmt.Sprintf("%s%s?region=%s&realm=%s&name=%s&fields=raid_progression", rioAPI, "guilds/profile", guild.Region, guild.RealmSlug, urlPkg.PathEscape(guild.Name))
	req, err := http.NewRequest("GET", url, nil)

	if err != nil {
		log.Println("[parse/raider.io] couldn't connect", err)
		return nil, err
	}

	res, getErr := client.Do(req)
	if getErr != nil {
		log.Println("[parse/raider.io]", getErr)
		return nil, getErr
	}

	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		log.Println("[parse/raider.io] couldn't read results", readErr)
		return nil, readErr
	}

	responseHolder := &RaiderIORaidProgressionResponse{}
	err = json.Unmarshal(body, responseHolder)
	if err != nil {
		log.Println("[parse/raider.io]", err)
		log.Println(string(body))
		return nil, err
	}

	return responseHolder, nil
}
