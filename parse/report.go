package parse

import (
	"time"

	"gitlab.com/rakshazi/warcraftlogs-api"
	"gitlab.com/rakshazi/wat/config"
)

// ParsedReport - processed data from WarcraftLogs report, fights and matched raiders
type ParsedReport struct {
	ID      string
	Date    time.Time
	Raiders []string
	Bosses  []*Boss
	Pulls   []*ParsedPull
}

// ParsedPull - info about any pull and raiders
type ParsedPull struct {
	ID              int    // warcraftlogs report's pull id
	Name            string //boss name
	Kill            bool
	BossPercentage  int
	FightPercentage int
	Difficulty      string //boss difficulty
	Raiders         []string
}

// Boss information from parsed report
type Boss struct {
	Name       string
	Difficulty string
	Killed     bool
	Tries      int
}

// Report - preprocess and parse report
func Report(guild *config.Item, report *warcraftlogs.Report, fight *warcraftlogs.Fight) *ParsedReport {
	raiders := Characters(guild, parseRaiders(fight))
	bosses, pulls := parseBosses(guild, fight)
	return &ParsedReport{
		ID:      report.Id,
		Date:    time.Unix(*report.StartTime/1000, 0),
		Raiders: raiders,
		Bosses:  bosses,
		Pulls:   pulls,
	}
}

// Get list of raiders from fight
func parseRaiders(fight *warcraftlogs.Fight) []string {
	var raiders []string
	for _, raider := range fight.Friendlies {
		if raider.Server == "" || raider.Type == "Unknown" || raider.Type == "" {
			continue
		}
		raiders = append(raiders, raider.Name)
	}

	return raiders
}

func getRaidersByPullID(guild *config.Item, friendlies []*warcraftlogs.Raider, id int) []string {
	var exists []string
	for _, friend := range friendlies {
		if friend.Server == "" || friend.Type == "Unknown" || friend.Type == "" {
			continue
		}
		for _, fight := range friend.Fights {
			if id != fight.Id {
				continue
			}
			exists = append(exists, friend.Name)
		}
	}
	return Characters(guild, exists)
}

// Parse bosses info and tries from report
func parseBosses(guild *config.Item, fight *warcraftlogs.Fight) ([]*Boss, []*ParsedPull) {
	var bosses []*Boss
	var pulls []*ParsedPull
	for _, pull := range fight.Fights {
		if isBoss(guild, pull) {
			bosses = appendMissingBoss(bosses, &Boss{
				Name:       pull.Name,
				Difficulty: getDifficultyText(pull.Difficulty),
				Killed:     pull.Kill,
				Tries:      1,
			})
			pulls = append(pulls, &ParsedPull{
				ID:              pull.Id,
				Name:            pull.Name,
				Kill:            pull.Kill,
				BossPercentage:  pull.BossPercentage,
				FightPercentage: pull.FightPercentage,
				Difficulty:      getDifficultyText(pull.Difficulty),
				Raiders:         getRaidersByPullID(guild, fight.Friendlies, pull.Id),
			})
		}
	}

	return bosses, pulls
}

// append boss if missing and increase tries counter on any attempt
func appendMissingBoss(slice []*Boss, boss *Boss) []*Boss {
	for i, item := range slice {
		if item.Name == boss.Name && item.Difficulty == boss.Difficulty {
			slice[i].Tries++
			if item.Killed == false && boss.Killed == true {
				slice[i].Killed = true
			}
			return slice
		}
	}
	return append(slice, boss)
}

// Check if that pull is a boss that should be included
func isBoss(guild *config.Item, pull *warcraftlogs.Pull) bool {
	// That's thrash
	if pull.Boss == 0 {
		return false
	}
	// That's ignored difficulty
	for _, ignoredDifficulty := range guild.IgnoreDifficulties {
		if pull.Difficulty == ignoredDifficulty {
			return false
		}
	}
	return true
}

// Convert WarcraftLogs Difficulty id to text
func getDifficultyText(difficulty int) string {
	switch difficulty {
	case 1:
		return "LFR"
	case 2:
		return "Flex"
	case 3:
		return "Normal"
	case 4:
		return "Heroic"
	case 5:
		return "Mythic"
	case 10:
		return "Challenge"
	}

	return "??"
}
