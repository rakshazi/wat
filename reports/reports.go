package reports

import (
	"log"
	sorter "sort"
	"time"

	"gitlab.com/rakshazi/warcraftlogs-api"
	"gitlab.com/rakshazi/wat/config"
	"gitlab.com/rakshazi/wat/parse"
)

// Download reports list (NOT fights) from API
func Download(api *warcraftlogs.WarcraftLogs, guild *config.Item) ([]*parse.ParsedReport, error) {
	reports := []*parse.ParsedReport{}
	list, err := api.ReportsForGuild(guild.Name, guild.Realm, guild.Region)
	if err != nil {
		return nil, err
	}
	log.Println("[reports] list has been downloaded for", guild.ID)

	// WarcraftLogs Report is only id/dates, fights are on separate endpoint
	for _, item := range cleanup(guild, list, guild.Raids) {
		fight, err := api.Fights(item.Id)
		if err != nil {
			return nil, err
		}
		reports = appendMissing(reports, parse.Report(guild, item, &fight))
	}
	// We'll count tries per boss by pulls on merged and cleaned reports
	for i, report := range reports {
		for idx, boss := range report.Bosses {
			reports[i].Bosses[idx].Tries = getTriesByBoss(report, boss)
		}
	}

	log.Println("[reports] have been parsed for", guild.ID)
	return sort(reports), nil
}

// Remove reports by filters
func cleanup(guild *config.Item, reports []*warcraftlogs.Report, raids []int64) []*warcraftlogs.Report {
	clean := []*warcraftlogs.Report{}
	for i, report := range reports {
		date := time.Unix(*report.StartTime/1000, 0)
		ignore := false
		// Ignore reports by day
		for _, day := range guild.IgnoreDays {
			if int64(date.Weekday()) == day {
				ignore = true
			}
		}
		// Ignore reports by zone
		for _, raidID := range raids {
			if raidID == report.Zone && !ignore {
				clean = append(clean, reports[i])
			}
		}
	}
	// Shrink reports to 2x size of maxRaids config (because some of reports will be merged later)
	size := 2 * guild.MaxRaids
	if len(clean) > size {
		clean = append([]*warcraftlogs.Report(nil), clean[:size]...)
	}

	return clean
}

// Sort log items by date
func sort(reports []*parse.ParsedReport) []*parse.ParsedReport {
	sorter.Slice(reports, func(i, j int) bool {
		return reports[i].Date.After(reports[j].Date)
	})
	return reports
}

// Merge append and merge reports
func appendMissing(reports []*parse.ParsedReport, report *parse.ParsedReport) []*parse.ParsedReport {
	merged := false
	for idx, item := range reports {
		if item.Date.Format("02/01") != report.Date.Format("02/01") {
			continue
		}
		reports[idx] = mergeReports(item, report)
		merged = true
	}
	if !merged {
		reports = append(reports, report)
	}

	return reports
}

// merge two reports into one
func mergeReports(base *parse.ParsedReport, item *parse.ParsedReport) *parse.ParsedReport {
	for _, raider := range item.Raiders {
		base.Raiders = parse.AppendMissing(base.Raiders, raider)
	}
	// Merge bosses
	for _, itemBoss := range item.Bosses {
		idx := hasBossPos(base, itemBoss)
		if idx > -1 { // if boss exists in base report
			// set killed
			if !base.Bosses[idx].Killed && itemBoss.Killed {
				base.Bosses[idx].Killed = true
			}
		} else { // if boss does NOT exist in base report
			base.Bosses = append(base.Bosses, itemBoss)
		}
	}

	// Merge pulls
	for _, itemPull := range item.Pulls {
		idx := hasPullPos(base, itemPull)
		if idx > -1 { // if pull exists in base report
			// Merge raiders
			for _, raider := range itemPull.Raiders {
				base.Pulls[idx].Raiders = parse.AppendMissing(base.Pulls[idx].Raiders, raider)
			}

		} else { // if pull does NOT exists in base report
			base.Pulls = append(base.Pulls, itemPull)
		}
	}
	return base
}

func hasBossPos(report *parse.ParsedReport, item *parse.Boss) int {
	for idx, boss := range report.Bosses {
		if boss.Name == item.Name && boss.Difficulty == item.Difficulty {
			return idx
		}
	}

	return -1
}

func hasPullPos(report *parse.ParsedReport, item *parse.ParsedPull) int {
	for idx, pull := range report.Pulls {
		if pull.Name == item.Name &&
			pull.Difficulty == item.Difficulty &&
			pull.BossPercentage == item.BossPercentage &&
			pull.FightPercentage == item.FightPercentage &&
			pull.Kill == item.Kill {
			return idx
		}
	}

	return -1
}

// Get count of tries on selected boss
func getTriesByBoss(report *parse.ParsedReport, boss *parse.Boss) int {
	tries := 0
	for _, pull := range report.Pulls {
		if boss.Name == pull.Name &&
			boss.Difficulty == pull.Difficulty {
			tries++
		}
	}

	return tries
}
