package warcraftlogs

import "fmt"

// Fight - actually, report
type Fight struct {
	Fights     []*Pull   `json:"fights,omitempty"`
	Friendlies []*Raider `json:"friendlies,omitempty"`
}

// Pull - fight/pull information
type Pull struct {
	Id                            int
	StartTime                     int    `json:"start_time"`
	EndTime                       int    `json:"end_time"`
	Boss                          int    `json:"boss,omitempty"`
	Name                          string `json:"name,omitempty"`
	Size                          int    `json:"size,omitempty"`
	Difficulty                    int    `json:"difficulty,omitempty"`
	Kill                          bool   `json:"kill,omitempty"`
	Partial                       int    `json:"partial,omitempty"`
	BossPercentage                int    `json:"bossPercentage,omitempty"`
	FightPercentage               int    `json:"fightPercentage,omitempty"`
	LastPhaseForPercentageDisplay int    `json:"lastPhaseForPercentageDisplay,omitempty"`
}

// Raider info
type Raider struct {
	Id     int
	Name   string
	Type   string
	Guid   int
	Server string
	Fights []struct {
		Id int
	}
}

// Fights - get fight from report by report id (code)
func (w *WarcraftLogs) Fights(code string) (Fight, error) {
	url := fmt.Sprintf("report/fights/%s", code)
	fight := Fight{}
	err := w.Get(url, &fight)

	return fight, err
}
