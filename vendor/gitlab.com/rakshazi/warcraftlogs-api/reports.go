package warcraftlogs

import "fmt"

type Report struct {
	Id        string
	Title     string
	Owner     string
	Zone      int64
	StartTime *int64 `json:"start"`
	EndTime   *int64 `json:"end"`
}

// ReportsForGuild returns a list of all reports available for provided guild.
// Warcraftlogs retention time of reports differs depending on reports tier.
func (w *WarcraftLogs) ReportsForGuild(guildName string, realm string, region string) ([]*Report, error) {

	url := fmt.Sprintf("reports/guild/%s/%s/%s", guildName, realm, region)

	return w.reportsFromUrl(url)
}

// ReportsForUser returns a list of all reports available for provided user.
// Warcraftlogs retention time of reports differs depending on reports tier.
func (w *WarcraftLogs) ReportsForUser(userName string) ([]*Report, error) {
	url := fmt.Sprintf("reports/user/%s", userName)

	return w.reportsFromUrl(url)
}

// Support function to get Reports from URL
func (w *WarcraftLogs) reportsFromUrl(url string) ([]*Report, error) {

	reports := []*Report{}
	err := w.Get(url, &reports)

	return reports, err
}
