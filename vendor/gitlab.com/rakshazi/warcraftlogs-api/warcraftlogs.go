package warcraftlogs

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"
)

// WarcraftLogs Client
type WarcraftLogs struct {
	client   http.Client
	apiToken string

	endpoint string
}

// New WarcraftLogs Client, with required API Token.
// Currently using v1 API endpoint as default
func New(apiToken string) *WarcraftLogs {

	api := &WarcraftLogs{
		endpoint: "https://www.warcraftlogs.com/v1/",
		client: http.Client{
			Timeout: time.Second * 60,
		},
		apiToken: apiToken,
	}

	return api
}

// SetEndpoint allows customizing API Endpoint that client will use.
// If provided endoint does not end with a trailing slash, it will be added
func (w *WarcraftLogs) SetEndpoint(endpoint string) {

	w.endpoint = endpoint

	if !strings.HasSuffix(w.endpoint, "/") {
		w.endpoint = w.endpoint + "/"
	}
}

// Support function to make an authenticated GET call and parse response JSON to a responseHolder.
func (w *WarcraftLogs) Get(path string, responseHolder interface{}) error {

	url := fmt.Sprintf("%s%s?api_key=%s", w.endpoint, path, w.apiToken)
	req, err := http.NewRequest("GET", url, nil)

	if err != nil {
		log.Println("Create request to WarcraftLogs error", err)
		return err
	}

	res, getErr := w.client.Do(req)
	if getErr != nil {
		log.Println(getErr)
		return getErr
	}
	if res.StatusCode != 200 {
		switch res.StatusCode {
		case 400:
			return fmt.Errorf("Not found on Warcraft Logs")
		case 429:
			log.Println("Too many requests to WarcraftLogs API, sleep 2 minutes")
			time.Sleep(2 * time.Minute)
			return w.Get(path, responseHolder)
		}
	}

	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		log.Println("HTTP Response from WarcraftLogs read failed ", readErr)
		return readErr
	}

	jsonErr := json.Unmarshal(body, responseHolder)
	if jsonErr != nil {
		log.Println("Unmarshaling JSON from WarcraftLogs to Go response struct failed", jsonErr)
		return jsonErr
	}

	return nil
}
