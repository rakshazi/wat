package warcraftlogs

type Zone struct {
	Id         int              `json:"id"`
	Name       string           `json:"name"`
	Frozen     bool             `json:"frozen"`
	Encounters []*ZoneEncounter `json:"encounters"`
	Brackets   *ZoneBrackets    `json:"brackets"`
}

// Zone Boss, id - warcraft logs internal id, NpcId - real id
type ZoneEncounter struct {
	Id    int    `json:"id"`
	NpcId int    `json:"npcID,omitempty"`
	Name  string `json:"name"`
}

// Zone requirements, type may be patch, ilvl, etc.
type ZoneBrackets struct {
	Min  float32 `json:"min"`
	Max  float32 `json:"max"`
	Type string  `json:"type"`
}

// Get zones from api
func (w *WarcraftLogs) Zones() ([]*Zone, error) {
	response := []*Zone{}
	err := w.Get("zones", &response)

	return response, err
}
